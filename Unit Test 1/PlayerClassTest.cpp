#include "stdafx.h"
#include "CppUnitTest.h"
#include "../QwirkleProject/Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PlayerClassTest
{
	TEST_CLASS(PlayerClassTest)
	{
	public:

		TEST_METHOD(TestConstructor)
		{
			Player newPlayer("TestName");
			if (newPlayer.getName() != "TestName" && newPlayer.getScore() != 0)
				Assert::Fail();

		}

		TEST_METHOD(addTileTest)
		{
			Player newPlayer;
			Tile newTile(Tile::Color::Red, Tile::Shape::Circle);
			newPlayer.addTile(newTile);
			if (newPlayer.getTileCount() != 1)
				Assert::Fail();
		}
		TEST_METHOD(chooseTileTest)
		{
			Player newPlayer;
			Tile newTile(Tile::Color::Red, Tile::Shape::Circle);
			newPlayer.addTile(newTile);
			if (!(newPlayer.ChooseTile(0) == newTile))
				Assert::Fail();
		}

		TEST_METHOD(removeFromHand)
		{
			Player newPlayer;
			Tile newTile(Tile::Color::Red, Tile::Shape::Circle);
			newPlayer.addTile(newTile);
			newPlayer.removeFromHand(0);
			if (newPlayer.getTileCount() != 0)
				Assert::Fail();
		}

	};
}