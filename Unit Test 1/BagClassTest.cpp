#include "stdafx.h"
#include "CppUnitTest.h"
#include "../QwirkleProject/Bag.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BagClassTest
{
	TEST_CLASS(BagClassTest)
	{
	public:

		TEST_METHOD(TestConstructor)
		{
			Bag bag;
			//std::cout << bag.showTilesSize();
			if (bag.showTilesSize() != 108)
				Assert::Fail();

		}

		TEST_METHOD(takeTilesTest)
		{
			Bag bag;
			std::vector<Tile> results;
			int nr = 5;
			results = bag.takeTiles(nr);
			if (bag.showTilesSize() != 103)
				Assert::Fail();
		}

		TEST_METHOD(takeTileTest)
		{
			Bag bag;
			Tile result;
			int nr = 5;
			result = bag.takeTile();
			if (bag.showTilesSize() != 107)
				Assert::Fail();
		}

		TEST_METHOD(addTileTest)
		{
			Bag bag;
			Tile newTile(Tile::Color::Red, Tile::Shape::Circle);
			bag.addTile(newTile);
			if (bag.showTilesSize() != 109)
				Assert::Fail();
		}
	};
}