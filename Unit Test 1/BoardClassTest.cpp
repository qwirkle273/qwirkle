#include "stdafx.h"
#include "CppUnitTest.h"
#include "../QwirkleProject/Board.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BoardClassTest
{
	TEST_CLASS(BoardClassTest)
	{
	public:

		TEST_METHOD(TestConstructor)
		{
			Board board;
			if ((board.getHeight() && board.getWidth()) != 1)
				Assert::Fail();
		}

		TEST_METHOD(CalculTest)
		{
			Board board;
			std::vector<Cordonate> coordonate;
			Cordonate coords = { 1,2 };
			coordonate.push_back(coords);
			if (board.calculateScore(coordonate) == 0)
				Assert::Fail();
		}

		TEST_METHOD(TileWithoutCheckTest)
		{
			Tile newTile(Tile::Color::Red, Tile::Shape::Circle);
			Board board;
			Cordonate coords1 = { 1,2 };
			Cordonate coords2 = { 1,3 };
			board.updateBoardLength(4);
			board.updateBoardLength(4);
			board.updateBoardLength(1);
			board.updateBoardLength(1);
			board.updateBoardLength(2);
			board.updateBoardLength(2);
			board.updateBoardLength(3);
			board.updateBoardLength(3);

			board.addTileWithoutCheck(coords1, newTile);
			board.addTileWithoutCheck(coords2, newTile);
			Assert::IsTrue(board.getMatrix()[coords1.first][coords1.second] == newTile);
		}

		TEST_METHOD(addTileTest)
		{
			Tile newTile(Tile::Color::Red, Tile::Shape::Circle);
			Board board;
			Cordonate coords1 = { 1,2 };
			Cordonate coords2 = { 1,3 };
			board.updateBoardLength(4);
			board.updateBoardLength(4);
			board.updateBoardLength(1);
			board.updateBoardLength(1);
			board.updateBoardLength(2);
			board.updateBoardLength(2);
			board.updateBoardLength(3);
			board.updateBoardLength(3);

			board.addTile(coords1, newTile);
			board.addTile(coords2, newTile);
			Assert::IsFalse(board.getMatrix()[coords1.first][coords1.second] == newTile);
		}

		TEST_METHOD(isInBoardTest)
		{
			Cordonate coords = { 2,2 };
			Board board;
			Assert::IsFalse(board.isInboard(coords));
		}
		TEST_METHOD(isInBoardTestwithXY)
		{
			int x = 1, y = 2;
			Cordonate coords = { 2,2 };
			Board board;
			Assert::IsFalse(board.isInboard(x,y));
		}

		TEST_METHOD(hasValueTest)
		{
			Board board;
			Cordonate coords = { 1,2 };
			board.updateBoardLength(4);
			board.updateBoardLength(4);
			board.updateBoardLength(1);
			board.updateBoardLength(1);
			board.updateBoardLength(2);
			board.updateBoardLength(2);
			board.updateBoardLength(3);
			board.updateBoardLength(3);
			Tile newTile(Tile::Color::Red, Tile::Shape::Circle);
			board.addTileWithoutCheck(coords, newTile);
			if (!board.hasValue(coords))
				Assert::Fail();

		}
		TEST_METHOD(hasValueTestWithXY)
		{
			Board board;
			int x = 1, y = 2;
			Cordonate coords = { x,y };
			board.updateBoardLength(4);
			board.updateBoardLength(4);
			board.updateBoardLength(1);
			board.updateBoardLength(1);
			board.updateBoardLength(2);
			board.updateBoardLength(2);
			board.updateBoardLength(3);
			board.updateBoardLength(3);
			Tile newTile(Tile::Color::Red, Tile::Shape::Circle);
			board.addTileWithoutCheck(coords, newTile);
			if (board.hasValue(x,y))
				Assert::Fail();

		}

		TEST_METHOD(isLegalTest)
		{
			Tile firstTile(Tile::Color::Red, Tile::Shape::Circle);
			Tile secondTile(Tile::Color::Blue, Tile::Shape::Circle);
			Board board;
			Assert::IsTrue(board.islegal(firstTile, secondTile));
		}

		TEST_METHOD(isValidTest)
		{
			Board board;
			Cordonate coords = { 1,2 };
			Tile newTile(Tile::Color::Red, Tile::Shape::Circle);
			board.updateBoardLength(4);
			board.updateBoardLength(4);
			board.updateBoardLength(1);
			board.updateBoardLength(1);
			board.updateBoardLength(2);
			board.updateBoardLength(2);
			board.updateBoardLength(3);
			board.updateBoardLength(3);
		//	board.addTileWithoutCheck(coords, newTile);
			Assert::IsFalse(board.isValid(coords, newTile));
		}

	};
}