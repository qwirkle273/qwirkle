#include "stdafx.h"
#include "CppUnitTest.h"
#include "../QwirkleProject/Tile.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TileClassTest
{		
	TEST_CLASS(TileClassTest)
	{
	public:
		
		TEST_METHOD(TestConstructor)
		{
			Tile newTile(Tile::Color::Red, Tile::Shape::Circle);
			Assert::IsTrue(Tile::Color::Red == newTile.getColor());
			Assert::IsTrue(Tile::Shape::Circle == newTile.getShape());
		}

	};
}