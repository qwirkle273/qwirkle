#pragma once
#include <dos.h>
#include "Player.h"
#include "Bag.h"
#include "Board.h"
#include <vector>

class Game
{
private:
	bool firstRound;
	Board board;
	Bag bag;
	std::vector<Player> players;
	Player* currentPlayer = NULL;
public:
	~Game();
	void play();
	void init();
	Player* getNextPlayer();
	int getPlayerOption();
	bool getTilesForPlayer(Player* player);
	void useTileForPlayer(Player* player);
	void changeTiles(Player* player);
	std::vector<Player> getPlayers();
	void showScor();
	void showTable();
	void showTiles(const Player* player);
};