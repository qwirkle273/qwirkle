#pragma once
#include<iostream>
class Tile
{
public:

	enum class Color : uint8_t
	{
		Red,
		Orange,
		Yellow,
		Green,
		Blue,
		Purple,
	};
	enum class Shape : uint8_t
	{
		Circle,
		Xshape,
		Diamond,
		Square,
		Star,
		Plus,
	};
public:
	Tile();
	~Tile();
	Tile(Color color, Shape shape);
private:
	int id;
	Color m_color : 3;
	Shape m_shape : 3;

public:
	Color getColor()const;
	Shape getShape()const;

public:
	std::string toString()const;
	friend std::ostream& operator <<(std::ostream&, const Tile);
	friend bool operator == (const Tile tile1, const Tile tile2);
	void printConsole();
};