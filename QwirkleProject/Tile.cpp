#include "Tile.h"
#include<string>
#include<Windows.h>
Tile::Tile()
{
}


Tile::~Tile()
{
}

Tile::Tile(Color color, Shape shape) :
	m_color(color),
	m_shape(shape)
{
	//Empty
}

Tile::Color Tile::getColor() const
{
	return m_color;
}

Tile::Shape Tile::getShape() const
{
	return m_shape;
}


std::string Tile::toString() const
{
	std::string output = "";
	output += "Color: ";
	switch (m_color)
	{
	case Color::Blue:
		output += "Blue";
		break;
	case Color::Green:
		output += "Green";
		break;
	case Color::Red:
		output += "Red";
		break;
	case Color::Yellow:
		output += "Yellow";
		break;
	case Color::Purple:
		output += "Purple";
		break;
	case Color::Orange:
		output += "Orange";
		break;
	}
	output += " Shape: ";
	switch (m_shape)
	{
	case Shape::Circle:
		output += "Circle";
		break;
	case Shape::Xshape:
		output += "Xshape";
		break;
	case Shape::Diamond:
		output += "Diamond";
		break;
	case Shape::Square:
		output += "Square";
		break;
	case Shape::Star:
		output += "Star";
		break;
	case Shape::Plus:
		output += "Plus";
		break;
	}
	return output;
}

void Tile::printConsole()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	int color = 15;
	switch (m_color)
	{
	case Color::Blue:
		color = 9;
		break;
	case Color::Green:
		color = 10;
		break;
	case Color::Red:
		color = 12;
		break;
	case Color::Yellow:
		color = 14;
		break;
	case Color::Purple:
		color = 13;
		break;
	case Color::Orange:
		color = 11;
		break;
	}
	std::string output;
	switch (m_shape)
	{
	case Shape::Circle:
		output += "O";
		break;
	case Shape::Xshape:
		output += "X";
		break;
	case Shape::Diamond:
		output += "D";
		break;
	case Shape::Square:
		output += 254;
		break;
	case Shape::Star:
		output += "*";
		break;
	case Shape::Plus:
		output += "+";
		break;
	}
	SetConsoleTextAttribute(hConsole, color);
	std::cout << output;
	SetConsoleTextAttribute(hConsole, 7);
}

std::ostream & operator<<(std::ostream& os, const Tile tile)
{
	os << tile.toString();
	return os;
}

bool operator==(const Tile tile1, const Tile tile2)
	{
		if (tile1.getColor() == tile2.getColor() && tile2.getShape() == tile2.getShape())
			return true;
		return false;
	}



