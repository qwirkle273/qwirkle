#pragma once
#include<vector>
#include<optional>
#include"Tile.h"

typedef std::pair<int, int> Cordonate;

class Board
{
private:
	std::vector<std::vector<std::optional<Tile>>>m_boardMatrix;
	int m_width;
	int m_height;

public:
	Board();
	~Board();
public:
	int  calculateScore(std::vector<Cordonate>cordonate);//
	bool addTileWithoutCheck(Cordonate cordonate, Tile &tile);//
	bool isValid(Cordonate cordonate, Tile &tile); //checks if tile can be placed at cordinates//
	bool addTile(Cordonate cordonate, Tile &tile);
	void printConsole();
	int getWidth();
	int getHeight();
	std::vector<std::vector<std::optional<Tile>>> getMatrix();
	bool hasValue(Cordonate cordonate);//
	bool hasValue(int x, int y);//
	bool isInboard(Cordonate cordonate);//
	bool isInboard(int x, int y);//
	bool islegal(std::optional<Tile>a, std::optional<Tile>b);//checks if only 1 of shape or color are equal//
	void updateBoardLength(int flag);//1-nord 2-west 3-south 4-east

};