#include "Board.h"
#include<iterator>
#include<array>
#include<iostream>

Board::Board()
{
	m_height = m_width = 1;
	m_boardMatrix.resize(1);
	m_boardMatrix[0].resize(1);
}

Board::~Board()
{
}

int Board::calculateScore(std::vector<Cordonate>cordonates)
{
	int score = 0;
	if (cordonates.size() > 1)
	{
		std::array<Cordonate, 2>neighbors;
		std::array<Cordonate, 2>line;
		bool sameLine = false;
		bool sameColumn = false;
		if (cordonates[0].first == cordonates[1].first)
		{
			sameLine = true;
			neighbors[0] = std::make_pair(0, 1);
			neighbors[1] = std::make_pair(0, -1);
			line[0] = std::make_pair(1, 0);
			line[1] = std::make_pair(-1, 0);
		}
		else
		{
			sameColumn = true;
			neighbors[0] = std::make_pair(1, 0);
			neighbors[1] = std::make_pair(-1, 0);
			line[0] = std::make_pair(0, 1);
			line[1] = std::make_pair(0, -1);
		}
		int count;
		Cordonate currentTile;
		for (auto cordonate : cordonates)
		{
			count = 1;
			for (auto neighbor : neighbors)
			{

				currentTile.first = cordonate.first + neighbor.first;
				currentTile.second = cordonate.second + neighbor.second;

				Cordonate nextTile;
				while (isInboard(currentTile) && hasValue(currentTile))
				{
					count++;
					nextTile.first = currentTile.first + neighbor.first;
					nextTile.second = currentTile.second + neighbor.second;
					currentTile = nextTile;
				}
			}
			score += count;
			if (count == 6)
				score += count;

		}
		count = 1;

		for (auto neighbor : line)
		{

			currentTile.first = cordonates.back().first + neighbor.first;
			currentTile.second = cordonates.back().second + neighbor.second;

			Cordonate nextTile;
			while (isInboard(currentTile) && hasValue(currentTile))
			{
				count++;
				nextTile.first = currentTile.first + neighbor.first;
				nextTile.second = currentTile.second + neighbor.second;
				currentTile = nextTile;
			}

		}
		score += count;
		if (count == 6)
			score += count;

	}
	else
	{

		std::array<Cordonate, 4>neighbors = { std::make_pair(1,0),std::make_pair(0,1),std::make_pair(-1,0),std::make_pair(0,-1) };
		Cordonate currentTile;
		for (auto neighbor : neighbors)
		{
			int count = 1;
			currentTile.first = cordonates.back().first + neighbor.first;
			currentTile.second = cordonates.back().second + neighbor.second;

			Cordonate nextTile;
			while (isInboard(currentTile) && hasValue(currentTile))
			{
				count++;
				nextTile.first = currentTile.first + neighbor.first;
				nextTile.second = currentTile.second + neighbor.second;
				currentTile = nextTile;
			}
			score += count;
			if (count == 6)
				score += count;
		}
	}
	return score;
}

bool Board::addTileWithoutCheck(Cordonate  cordonate, Tile & tile)
{
	if (isInboard(cordonate))
	{
		m_boardMatrix[cordonate.first][cordonate.second] = tile;
		if (cordonate.first >= m_height - 1)
			updateBoardLength(3);
		if (cordonate.first <= 0)
			updateBoardLength(1);
		if (cordonate.second >= m_width - 1)
			updateBoardLength(4);
		if (cordonate.second <= 0)
			updateBoardLength(2);
		return true;
	}
	else return false;

}

bool Board::isValid(Cordonate cordonate, Tile & tile)
{
	if (!isInboard(cordonate))
	{
		std::cout << "out of bounds\n";
		return false;
	}
	if (hasValue(cordonate))
	{
		std::cout << "Este deja piesa in locul acela\n";
		return false;
	}
	std::array<Cordonate, 4>neighbors = { std::make_pair(1,0),std::make_pair(0,1),std::make_pair(0,-1),std::make_pair(-1,0) };
	bool hasNeighbour = false;
	for (auto neighbor : neighbors)
	{
		if (isInboard(cordonate.second + neighbor.second, cordonate.first + neighbor.first) && hasValue(cordonate.second + neighbor.second, cordonate.first + neighbor.first))
		{
			Cordonate currentTile;
			currentTile.first = cordonate.first + neighbor.first;
			currentTile.second = cordonate.second + neighbor.second;

			Cordonate nextTile;
			while (isInboard(currentTile) && hasValue(currentTile))
			{
				if (islegal(tile, m_boardMatrix[currentTile.first][currentTile.second]))
				{
					hasNeighbour = true;
					nextTile.first = currentTile.first + neighbor.first;
					nextTile.second = currentTile.second + neighbor.second;
					currentTile = nextTile;
				}
				else
				{
					std::cout << "nu este legal\n";
					return false;
				}

			}
		}
	}
	if (!hasNeighbour)
	{
		std::cout << "nu are vecini";
		return false;
	}
}

bool Board::addTile(Cordonate cordonate, Tile & tile)
{
	if (isValid(cordonate, tile))
	{
		m_boardMatrix[cordonate.first][cordonate.second] = tile;
		if (cordonate.first >= m_height - 1)
			updateBoardLength(3);
		if (cordonate.first <= 0)
			updateBoardLength(1);
		if (cordonate.second >= m_width - 1)
			updateBoardLength(4);
		if (cordonate.second <= 0)
			updateBoardLength(2);
		return true;
	}
	return false;
}

bool Board::hasValue(Cordonate cordonate)
{
	return m_boardMatrix[cordonate.first][cordonate.second].has_value();
}

bool Board::hasValue(int x, int y)
{
	return m_boardMatrix[y][x].has_value();
}

bool Board::isInboard(Cordonate cordonate)
{
	return cordonate.first >= 0 && cordonate.first < m_height &&cordonate.second >= 0 && cordonate.second < m_width;
}

bool Board::isInboard(int x, int y)
{
	return y >= 0 && y < m_height &&x >= 0 && x < m_width;
}

bool Board::islegal(std::optional<Tile> a, std::optional<Tile> b)
{
	bool sameShape = false;
	bool sameColor = false;
	if (a->getColor() == b->getColor())
	{
		sameColor = true;
	}
	if (a->getShape() == b->getShape())
	{
		sameShape = true;
	}
	return sameShape ^ sameColor;
}

void Board::updateBoardLength(int flag)
{
	if (flag % 2 == 1)
	{
		auto iterator = m_boardMatrix.begin();
		if (flag == 3)
		{
			iterator = m_boardMatrix.end();
		}
		std::vector<std::optional<Tile>> newVector;
		newVector.resize(m_width);
		m_boardMatrix.insert(iterator, newVector);
		m_height++;
	}
	if (flag % 2 == 0)
	{
		for (int indexLinie = 0; indexLinie < m_height; indexLinie++)
		{
			auto iterator = m_boardMatrix[indexLinie].begin();

			if (flag == 4)
			{
				iterator = m_boardMatrix[indexLinie].end();
			}
			std::optional<Tile>newTile;
			m_boardMatrix[indexLinie].insert(iterator, newTile);
		}
		m_width++;
	}

}

void Board::printConsole()
{
	std::cout << std::endl;
	std::cout << "  ";
	for (int i = 65; i < m_boardMatrix[0].size() + 65; i++) {
		std::cout << (char)i << " ";
	}
	std::cout << std::endl;
	int lineCnt = 0;
	for (auto line : m_boardMatrix)
	{
		std::cout << lineCnt++ << " ";
		for (auto column : line)
		{

			if (column.has_value())
				column->printConsole();
			else
				std::cout << "_";
			std::cout << " ";
		}
		std::cout << std::endl;
	}

	std::cout << std::endl;
	//	std::cout << " Legenda " << std::endl;
}

int Board::getWidth()
{
	return m_width;
}

int Board::getHeight()
{
	return m_height;
}

std::vector<std::vector<std::optional<Tile>>> Board::getMatrix()
{
	return m_boardMatrix;
}



