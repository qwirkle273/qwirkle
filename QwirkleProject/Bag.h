#pragma once
#include<iostream>
#include"Tile.h"
#include<vector>
#include <random>
#include <ctime>

class Bag {
public:
	Bag();
	void showTiles();
	std::vector<Tile> Bag::takeTiles(int nr);//algoritm scoatere piese din sac
	Tile takeTile();
	int showTilesSize();
	void addTile(Tile tile);
private:
	std::vector<Tile> tiles;
	void initialize();//initializare elemente(toate exista in sac)
};