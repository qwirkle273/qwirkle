#include "Game.h"

Game::~Game()
{
}

void Game::init()
{
	firstRound = true;
	std::cout << "Welcome to Quirkle" << std::endl;
	std::cout << "Enter number of players" << std::endl;
	int playerNo = 0;
	std::cin >> playerNo;
	for (int i = 0; i < playerNo; i++) {
		std::cout << "Enter player " << i + 1 << " name: ";
		std::string playerName;
		std::cin >> playerName;
		Player p(playerName);
		players.push_back(p);
	}

	if (playerNo < 2 || playerNo >= 4) {
		throw "Invalid number of players";
	}
}

int Game::getPlayerOption()
{
	std::cout << std::endl;
	std::cout << "1) Change tiles ";
	std::cout << "2) Use tiles. ";
	std::cout << "3) End round. ";
	std::cout << "4) End game. " << std::endl;
	std::cout << "Option: ";
	int userOption = 0;
	std::cin >> userOption;
	return userOption;
}

Player* Game::getNextPlayer()
{
	if (currentPlayer == NULL)
	{
		// set currentPlayer to the first one
		currentPlayer = &players[0];
	}
	else
	{
		// take next player
		int playerIdx = -1;
		for (int i = 0; i < players.size(); i++) {
			if (players[i].getName() == currentPlayer->getName()) {
				playerIdx = i;
				break;
			}
		}
		if (playerIdx == players.size() - 1) {
			// take the first one
			currentPlayer = &players[0];
		}
		else {
			// take the next one
			currentPlayer = &players[playerIdx + 1];
		}
	}
	return currentPlayer;
}

bool Game::getTilesForPlayer(Player* player) {
	int tilesNo = 6 - player->getTileCount();
	std::vector<Tile> tiles = bag.takeTiles(6 - player->getTileCount());
	for (auto tile : tiles) {
		player->addTile(tile);

	}
	if (player->getTileCount() == 6)
	{
		return true;
	}
	else false;
}

void Game::useTileForPlayer(Player* player) {
	bool inserted = false;
	int indexTile;
	Cordonate cordonateTile;
	int newInsert = 1;
	Tile auxTile;
	std::vector<Cordonate> insertedTiles;
	if (firstRound)
	{
		std::cout << "alegeti prima piesa\n";
		std::cin >> indexTile;
		auxTile = player->ChooseTile(indexTile);
		player->removeFromHand(indexTile);
		board.addTileWithoutCheck(std::make_pair(0, 0), auxTile);
		board.printConsole();
		inserted = true;
		firstRound = false;
	}
	while (newInsert != 0)
	{
		player->printTiles();
		inserted = false;
		std::cout << "alegeti piesa\n";
		std::cin >> indexTile;
		auxTile = player->ChooseTile(indexTile);
		std::cout << "piesa aleasa:";
		auxTile.printConsole();
		std::cout << "\n";
		std::cout << "alegeti locul\n";
		std::cin >> cordonateTile.first >> cordonateTile.second;
		if (insertedTiles.size() == 0)
		{

			inserted = board.addTile(cordonateTile, auxTile);
			if (inserted)
			{
				insertedTiles.push_back(cordonateTile);
				player->removeFromHand(indexTile);
			}
		}
		else
		{
			bool sameLine = false;
			bool sameColumn = false;
			if (cordonateTile.first == insertedTiles[0].first)
			{
				sameLine = true;
			}
			if (cordonateTile.second == insertedTiles[0].second)
			{
				sameColumn = true;
			}
			if (!(sameLine^sameColumn))
			{
				std::cout << "invalid\n";
			}
			else
			{
				inserted = board.addTile(cordonateTile, auxTile);
				if (inserted)
				{
					insertedTiles.push_back(cordonateTile);
					player->removeFromHand(indexTile);
				}

			}



		}
		board.printConsole();
		std::cout << "insert nou?0/1\n";
		std::cin >> newInsert;
	}
	if (inserted)
		player->setScore(player->getScore() + board.calculateScore(insertedTiles));
}

void Game::changeTiles(Player* player)
{
	Tile auxTile;
	int raspuns = 1;
	int indexTile;
	{
		player->printTiles();
		std::cout << "alege piesa ce vrei sa o scoti\n";
		std::cin >> indexTile;
		auxTile = player->ChooseTile(indexTile);
		player->removeFromHand(indexTile);
		bag.addTile(auxTile);
		std::cout << "din nou 0/1?\n";
		std::cin >> raspuns;

	}
	getTilesForPlayer(player);

}

void Game::play()
{
	Player* player;
	bool firstRound = false;
	bool gameRunning = true;

	while (gameRunning) {
		system("CLS");

		// 0. take next player
		player = getNextPlayer();

		// 1. print console
		board.printConsole();
		if (bag.showTilesSize() > 0)
		{
			getTilesForPlayer(player);
		}
		// 2. print current player
		std::cout << "Playing " << player->getName() << " score:" << player->getScore() << std::endl;
		player->printTiles();


		// 2. user choice
		int userOption = getPlayerOption();
		player->printTiles();
		switch (userOption) {
		case 1:
			// take tiles
			changeTiles(player);

			break;
		case 2:
			// use tiles
			useTileForPlayer(player);
			break;
		case 3:
			// end round
			break;
		case 4:
			// end game
			gameRunning = false;
			break;
		}
	}
}

std::vector<Player> Game::getPlayers() {
	return players;
}

void Game::showScor() {

}

void Game::showTable() {
	Bag t;
	t.takeTiles(6);
	t.showTiles();

	std::cout << std::endl;
	//t.colorare();
	std::cout << std::endl;
	Board b;
	std::vector<Tile>ceva = t.takeTiles(5);
	std::vector<Cordonate> cor = { {0,0}, {0,1}, {2,2}, {3,1}, {2,0} };
	int index = 0;
	for (auto tile : ceva)
	{
		b.printConsole();
		std::cout << "\n";
		b.addTileWithoutCheck(cor[index++], tile);
		b.printConsole();
		std::cout << "\n============\n";
	}
}