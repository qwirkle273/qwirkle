#pragma once
#include <string>
#include "Tile.h"
#include <vector>
#include <fstream>

class Player
{
private:
	int m_score;
	std::string m_name;
	std::vector<Tile> m_hand;
public:
	Player();
	~Player();
	Player(std::string name);
public:
	void setName(std::string name);
	std::string getName()const;
	int getScore()const;
	void printTiles();
	void addTile(Tile tile);
	int getTileCount();
	Tile& ChooseTile(int indexTile);
	void removeFromHand(int indexTile);
	void setScore(int score);
private:

	void showTilesInFile();
public:
	friend std::ostream& operator <<(std::ostream& os, const Player player);
};

