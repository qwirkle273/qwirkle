#include <iostream>
#include "Bag.h"
#include "Tile.h"
#include <vector>
#include <random>
#include <ctime>
#include <cstdlib>
#include <dos.h>

Bag::Bag()
{
	initialize();
}

void Bag::initialize()
{
	std::random_device rd;
	std::mt19937 gen(rd()); //standard mersenne_twister_engine seeded with rd()
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 6; j++)
			for (int k = 0; k < 6; k++)
			{
				Tile tile = Tile((Tile::Color)j, (Tile::Shape)k);
				tiles.push_back(tile);
			}
	std::shuffle(tiles.begin(), tiles.end(), rd);
}

Tile Bag::takeTile()
{
	Tile result;

	result = tiles.back();
	tiles.pop_back();
	return result;
}

int Bag::showTilesSize()
{
	return tiles.size();
}

void Bag::addTile(Tile tile)
{
	std::random_device rd;
	std::mt19937 gen(rd()); //standard mersenne_twister_engine seeded with rd()
	std::uniform_int_distribution<>distr(0, tiles.size() - 1);
	int index = distr(rd);
	tiles.insert(tiles.begin()+index, tile);
}

std::vector<Tile> Bag::takeTiles(int nr)
{
	std::vector<Tile> results;
	for (int index = 0; index < nr; index++)
	{
		if (tiles.size() > 0)
		{
			results.push_back(tiles.back());
			tiles.pop_back();
		}
	}
	return results;
}

void Bag::showTiles()
{
	int nr = 3;
	std::vector <Tile> a;
	a = takeTiles(nr);
	for (int i = 0; i < a.size(); i++) {
		a[i].printConsole();
		std::cout << "\n";
	}
}