#include <iostream>
#include <fstream>
//#include "Tile.h"
#include "Logging.h"
//#include "Bag.h"
//#include "board.h"
#include <dos.h>
#include "Game.h"
#define Cordonate std::pair<int,int>

void startQuirkle() {
	Game game;
	try {
		game.init();
		game.play();
		std::cout << "End game" << std::endl;
	} catch (std::string err) {
		std::cout << err << std::endl;
		system("pause");
		exit(2);
	}
}

int main()
{
	startQuirkle ();
	system("pause");
}

void testingLoggingSystem() {
	Logger log = Logger(std::cout);
	log.log("Initializing...", Logger::Level::Info);

	log.log("Testing info level logging...", Logger::Level::Info);
	log.log("Testing warning level logging...", Logger::Level::Warning);

	try {
		std::string("abc").substr(10);
	}
	catch (const std::exception& e) {
		log.log(e.what(), Logger::Level::Error);
	}

	std::ofstream file;
	file.open("log.txt");
	Logger flog = Logger(file);
	flog.log("Testing info level logging...", Logger::Level::Info);
	flog.log("Testing warning level logging...", Logger::Level::Warning);
	flog.log("Testing error level logging...", Logger::Level::Error);
	file.close();

	std::cout << "... Running main program ..." << std::endl;
}
