#include "Player.h"



Player::Player()
{
}


Player::~Player()
{
}

Player::Player(std::string name) :
	m_name(name),
	m_score(0)
{
	//empty
}

void Player::setName(std::string name)
{
	m_name = name;
}

std::string Player::getName() const
{
	return m_name;
}

int Player::getScore() const
{
	return m_score;
}

void Player::setScore(int score)
{
	m_score = score;
}

void Player::printTiles()
{
	std::cout << "Your tiles:" << std::endl;
	for (auto tile : m_hand) {
		// std::cout << tile << std::endl;
		tile.printConsole();
	}
	std::cout << std::endl;
}

int Player::getTileCount()
{
	return m_hand.size();
}

Tile & Player::ChooseTile(int indexTile)
{
	auto &aux=m_hand[indexTile];

	return aux;

}

void Player::removeFromHand(int indexTile)
{
	m_hand.erase(m_hand.begin() + indexTile);
}

void Player::addTile(Tile tile) {
	m_hand.push_back(tile);
}

void Player::showTilesInFile()
{
	std::ofstream f;
	f.open(m_name.c_str());
	for (int i = 0; i < m_hand.size(); i++)
		std::cout << m_hand[i] << " ";
	std::cout << std::endl << "Your score is: " << m_score;
	f.close();
}

std::ostream & operator<<(std::ostream& os, const Player player)
{
	os << player.m_name << " Score: " << player.m_score;
	os << std::endl << "Hand:" << std::endl;
	for (auto tile : player.m_hand)
		os << tile << std::endl;
	return os;
}